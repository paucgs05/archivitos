
#include "lista.h"
#include <assert.h>  

using namespace std;

Lista::Lista(void){
	primero=NULL;
}

Lista::Lista(int v[], unsigned int size){

	// Si vector no tiene elementos se crea la lista vacia
	if (size == 0) primero = NULL;
	else{
		// Se agrega el primer elemento
		Nodo* pnodo = new Nodo;
		pnodo->elemento = v[0];
		pnodo->proximo = NULL;
		primero = pnodo;	
		// Se agregan el resto de los elementos
		for(unsigned int i = 1; i < size;i++){
			Nodo* pactual = pnodo;		
			pnodo = new Nodo;
			pnodo->elemento = v[i];   
			pnodo->proximo = NULL;
			pactual->proximo = pnodo;
		}
	}
}

Lista::~Lista(){
	Nodo* pnodo = this->primero;
	while(pnodo!=NULL){
		Nodo* pprox = pnodo->proximo;
		delete pnodo;
		pnodo=pprox;
	}
}

void Lista::mostrar(ostream &os) const{
	Nodo* pactual = primero;
	os << "[";
	while(pactual!=NULL) {
		os << pactual->elemento;
		pactual=pactual->proximo;
		if (pactual!=NULL) os << ",";
	}
	os << "]" << endl;
}

ostream & operator<<(ostream &os, const Lista& lista){
	lista.mostrar(os);	  
	return os;
}

bool Lista::operator==(const Lista& l){
	Nodo* pactual_this = this->primero;
	Nodo* pactual_l = l.primero;

	while(pactual_this != NULL && pactual_l != NULL){
		if (pactual_this->elemento != pactual_l->elemento) return false;
		pactual_this = pactual_this->proximo;
		pactual_l = pactual_l->proximo;
	}	
	return (pactual_this == pactual_l);
}

void Lista::agregarAdelante(int x){
	Nodo* nNodo = new Nodo;
	nNodo -> elemento = x;
	nNodo -> proximo = primero;
	primero = nNodo;
	
	
}

void Lista::agregarAtras(int x){
	Nodo* nNodo = new Nodo;
	nNodo -> elemento = x;
	nNodo -> proximo = NULL;
	if (primero == NULL) {
		primero = nNodo;
	} else {
		Nodo* n = primero;
		while ((n -> proximo) != NULL) {
			n = n-> proximo;}
			
			n -> proximo = nNodo;

	}
}

unsigned int Lista::longitud(void) const{
	unsigned int res = 0;
	if (primero != NULL) {
		res = 1;
		Nodo* nodo = primero;
		while ((nodo -> proximo) != NULL){
			res++;
			nodo = nodo -> proximo;
		}		
}
return res;
}


bool Lista::pertenece(int x) const{
	bool res = false;
	if (primero != NULL){
		Nodo* nodoA = primero;
		while (nodoA != NULL) {
			if (nodoA -> elemento == x){
			res = true;
			break;
		}
		nodoA = nodoA -> proximo;
	}
}
return res;
}


int Lista::iesimo(int i) const{
	Nodo* nodoI = primero;
	for (int j = 0; j < i; j++){
		nodoI = nodoI -> proximo;
	}
	return nodoI -> elemento;
}

int Lista::maximo(void) const{
	int max = primero->elemento;
	Nodo* nodoA = primero;
	while (nodoA -> proximo != NULL){
		if (nodoA -> elemento < nodoA -> proximo -> elemento)
		{ max = nodoA -> proximo -> elemento;}
		nodoA = nodoA -> proximo;
	}
	
	return max;
}

void Lista::borrar_iesimo(int i){
		Nodo* nodoI = primero;
	if (i == 0 ){

	primero = primero -> proximo;
	delete nodoI;
} else {
	for (int j = 0; j < i-1; j++)
	{		nodoI = nodoI -> proximo;
}
	
	Nodo* nodoABorrar = nodoI -> proximo;
	Nodo* nodoSiguiente = nodoI -> proximo -> proximo;
	nodoI -> proximo = nodoSiguiente;
	delete nodoABorrar;

}
}
		

void Lista::reversa(void){
Nodo* nodoI = primero;
Nodo* nodoJ = primero -> proximo;
while (nodoJ != NULL) {
	Nodo* nodoX = nodoI;
	nodoJ -> proximo = nodoX;
	nodoI = nodoI -> proximo;
}
}	


void Lista::dividir_pares_impares(void){

	
}

Lista Lista::copiar(void) const{
	return *this;
}
	
